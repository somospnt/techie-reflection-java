package com.somospnt.techiereflection.service.impl;

import com.somospnt.techiereflection.anotaciones.Inyectar;
import com.somospnt.techiereflection.dao.TechieDao;
import com.somospnt.techiereflection.service.ServiceTechie;
import java.util.List;


public class ServiceTechieImpl implements ServiceTechie {

    @Inyectar
    private TechieDao techieDao;

    @Override
    public List<Object> obtenerTodos() {
        return techieDao.obtenerTodos();
    }

}
