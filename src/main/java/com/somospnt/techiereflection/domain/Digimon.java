package com.somospnt.techiereflection.domain;

public class Digimon {

    private String nombre;
    private String peso;
    private String tipo;
    private String tipoElemental;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPeso() {
        return peso;
    }

    public void setPeso(String peso) {
        this.peso = peso;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipoElemental() {
        return tipoElemental;
    }

    public void setTipoElemental(String tipoElemental) {
        this.tipoElemental = tipoElemental;
    }

}
