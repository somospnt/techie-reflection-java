package com.somospnt.techiereflection.domain;

import java.util.List;

public class Medabot {

    private String nombre;
    private String tinPet;
    private List<String> listaDePartes;
    private String medalla;

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTinPet() {
        return tinPet;
    }

    public void setTinPet(String tinPet) {
        this.tinPet = tinPet;
    }

    public List<String> getListaDePartes() {
        return listaDePartes;
    }

    public void setListaDePartes(List<String> listaDePartes) {
        this.listaDePartes = listaDePartes;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

}
