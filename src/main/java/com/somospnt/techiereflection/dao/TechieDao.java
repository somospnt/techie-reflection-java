package com.somospnt.techiereflection.dao;

import java.util.List;

public interface TechieDao {

    List<Object> obtenerTodos();

}
