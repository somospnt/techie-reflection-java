package com.somospnt.techiereflection.dao.impl;

import com.somospnt.techiereflection.anotaciones.Frijol;
import com.somospnt.techiereflection.dao.TechieDao;
import com.somospnt.techiereflection.domain.Digimon;
import com.somospnt.techiereflection.domain.Medabot;
import com.somospnt.techiereflection.domain.Pokemon;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Frijol
public class TechieDaoImpl implements TechieDao {

    @Override
    public List<Object> obtenerTodos() {
        List<Object> infancia = new ArrayList<>();

        Digimon digimon = new Digimon();
        digimon.setNombre("Wargreymon");
        digimon.setPeso("20G");
        digimon.setTipo("Vacuna");
        digimon.setTipoElemental("Dragon");

        Medabot medabot = new Medabot();
        medabot.setNombre("Metabee");
        medabot.setMedalla("Kabuto");
        medabot.setTinPet("Male");
        medabot.setListaDePartes(Arrays.asList("KBT-11", "KBT-12", "KBT-13", "KBT-14"));

        Pokemon pokemon = new Pokemon();
        pokemon.setNombre("Charizard");
        pokemon.setTipo1("Fuego");
        pokemon.setTipo2("Volador");
        pokemon.setAtaques(Arrays.asList("Pulso Dragon", "Rayo Solar", "Lanzallamas", "Respiro"));

        infancia.add(digimon);
        infancia.add(medabot);
        infancia.add(pokemon);

        return infancia;
    }

}
