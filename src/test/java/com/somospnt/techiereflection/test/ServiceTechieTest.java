package com.somospnt.techiereflection.test;

import com.somospnt.techiereflection.anotaciones.Frijol;
import com.somospnt.techiereflection.anotaciones.Inyectar;
import com.somospnt.techiereflection.dao.TechieDao;
import com.somospnt.techiereflection.service.ServiceTechie;
import com.somospnt.techiereflection.service.impl.ServiceTechieImpl;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.util.List;
import java.util.Set;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.assertEquals;
import org.reflections.Reflections;

public class ServiceTechieTest {

    private ServiceTechie serviceTechie;

    @Before
    public void setUp() {
        serviceTechie = new ServiceTechieImpl();
    }

    public void prepararContexto() throws Exception {

        //Obtener el atributo techieDao dentro del objeto serviceTechie.
        Field techieDaoField = serviceTechie.getClass().getDeclaredField("techieDao");
        //Obtener la anotación.
        for (Annotation annotation : techieDaoField.getAnnotations()) {
            //Si es la notación Inyectar, que lo inyecte.
            if (annotation.annotationType().equals(Inyectar.class)) {
                techieDaoField.setAccessible(true);
                techieDaoField.set(serviceTechie, obtenerInstanciaDe(TechieDao.class));
                techieDaoField.setAccessible(false);
            }
        }

    }

    private Object obtenerInstanciaDe(Class interfaz) throws Exception {

        Reflections reflections = new Reflections("com.somospnt.techiereflection.dao");

        //Obtener todas las clases de esta interfaz (Usar Api provista por la libreria Reflections).
        Set<Class<? extends Object>> subClases = reflections.getSubTypesOf(interfaz);
        for (Class o : subClases) {
            if (o.getAnnotationsByType(Frijol.class) != null) {
                return o.newInstance();
            }
        }
        return null;
    }

    /*
        TechieDaoImpl devuelve 3 elementos
        Para hacer pasar el test, no modificar TechieDaoImpl.java ni ServiceTechieImpl.java
        Utilizar un proxy para interceptar la respuesta y agregar un elemento      
        http://docs.oracle.com/javase/6/docs/api/java/lang/reflect/Proxy.html
        Recordar pasarle al InvocationHandler la instancia que queremos proxear, en nuestro caso el TechieDaoImpl
     */
    @Test
    public void nuevoTestDeContexto() throws Exception {

        prepararContexto();
        List<Object> resultados = serviceTechie.obtenerTodos();
        assertEquals(4, resultados.size());
    }

}
